package com.greta94.golfs.models;

import java.util.Objects;

//Todo Javadoc
public class Parcours {
    private int id;
    private String name;
    private Hole holes;

    public Parcours(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Hole getHoles() {
        return holes;
    }

    public void setHoles(Hole holes) {
        this.holes = holes;
    }
    //Todo equals(), hashcode() et toString()

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Parcours)) return false;
        Parcours parcours = (Parcours) o;
        return getId() == parcours.getId() && getName().equals(parcours.getName()) && getHoles().equals(parcours.getHoles());
    }

    @Override
    public int hashCode() { return Objects.hash(id, name); }

    @Override
    public String toString() {
        return "Parcours{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", holes=" + holes +
                '}';
    }
}
