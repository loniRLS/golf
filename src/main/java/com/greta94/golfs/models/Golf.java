package com.greta94.golfs.models;

import java.util.ArrayList;

//Todo Javadoc
public class Golf {
    private int id;
    private String name;
    private ArrayList<Parcours> parcours;

    public Golf(int id, String name, ArrayList<Parcours> parcours) {
        this.id = id;
        this.name = name;
        this.parcours = parcours;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Parcours> getParcours() {
        return parcours;
    }

    public void setParcours(ArrayList<Parcours> parcours) {
        this.parcours = parcours;
    }
//Todo : Constructeur, getters, setter,equals(), hashcode() et toString()
}
