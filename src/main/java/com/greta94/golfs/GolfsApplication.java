package com.greta94.golfs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GolfsApplication {

	public static void main(String[] args) {
		SpringApplication.run(GolfsApplication.class, args);
	}

}
